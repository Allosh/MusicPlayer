package fr.axelserieys.musicplayer.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import fr.axelserieys.musicplayer.Notifications.MusicNotification;
import fr.axelserieys.musicplayer.Player.MusicPlayer;
import fr.axelserieys.musicplayer.R;
import fr.axelserieys.musicplayer.controllers.PlaylistActivity;
import fr.axelserieys.musicplayer.database.DatabaseHelper;
import fr.axelserieys.musicplayer.fragments.FragmentSongs;
import fr.axelserieys.musicplayer.models.Album;
import fr.axelserieys.musicplayer.models.Artist;
import fr.axelserieys.musicplayer.models.Playlist;
import fr.axelserieys.musicplayer.models.Song;
import fr.axelserieys.musicplayer.ui.SectionsPagerAdapter;

public class MainActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST = 1;
    public static MainActivity mainActivity;
    private static boolean bottomBarVisible = false;
    public static EditText searchInput;
    public static TabLayout tabs;

    public static List<Song> master_songs = new ArrayList<Song>();
    public static List<Song> songs = new ArrayList<Song>();
    public static List<Artist> artists = new ArrayList<Artist>();
    public static List<Album> albums = new ArrayList<Album>();
    public static List<Playlist> playlists = new ArrayList<Playlist>();

    public static MusicNotification musicNotificationManager;

    public static PlaylistActivity playlistActivity = new PlaylistActivity();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            if (shouldShowRequestPermissionRationale(
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                Toast.makeText(this, "La permission est nécessaire pour pouvoir chercher les titres sur le smartphone.", Toast.LENGTH_LONG).show();
            }

            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST);


            return;
        }

        if(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if(shouldShowRequestPermissionRationale(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                Toast.makeText(this, "La permission est nécessaire pour pouvoir chercher les titres sur le smartphone.", Toast.LENGTH_LONG).show();
            }
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
            return;
        }

        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
        mainActivity = this;

        searchInput = findViewById(R.id.textSearch);

        musicNotificationManager = MusicNotification.getInstance();

        DatabaseHelper.getInstance(); //Loads the database
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            finishAffinity();
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        } else {
            ActivityCompat.finishAffinity(MainActivity.this);
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        }
    }

    public void addPlaylist(View view) {
        Intent intent = new Intent(this, PlaylistActivity.class);
        startActivity(intent);
    }

    public static void toast(String message) {
        Toast.makeText(mainActivity, message, Toast.LENGTH_SHORT).show();
    }

    public static MainActivity getInstance() {
        return mainActivity;
    }

    @Override
    public void onResume() {
        super.onResume();

        //Reloads the bottom bar
        if(MusicPlayer.isPlaying) {
            ((ImageView) findViewById(R.id.bottom_pause)).setVisibility(View.VISIBLE);
            ((ImageView) findViewById(R.id.bottom_play)).setVisibility(View.GONE);
        } else {
            ((ImageView) findViewById(R.id.bottom_play)).setVisibility(View.VISIBLE);
            ((ImageView) findViewById(R.id.bottom_pause)).setVisibility(View.GONE);
        }
    }

    public void play(View view) {
        if(!bottomBarVisible)
            showBottomBar();
        findViewById(R.id.bottom_play).setVisibility(View.GONE);
        findViewById(R.id.bottom_pause).setVisibility(View.VISIBLE);
        MusicPlayer.play();

        musicNotificationManager.show();
    }

    public void pause(View view) {
        findViewById(R.id.bottom_play).setVisibility(View.VISIBLE);
        findViewById(R.id.bottom_pause).setVisibility(View.GONE);
        MusicPlayer.pause();
    }

    public void showBottomBar() {
        Toast.makeText(this, "show", Toast.LENGTH_SHORT).show();
        Log.d("overwatch", "show");
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                for(int i = 0; i <= 200; i+=50) {
                    findViewById(R.id.bottom_content).setBottom(200);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        //thread.start();

        findViewById(R.id.bottom_content).setBottom(200);
        bottomBarVisible = true;
    }

    public void hideBottomBar() {
        findViewById(R.id.bottom_content).setBottom(-200);
        bottomBarVisible = false;
    }

    public void bottomClick(View view) {
        Intent intent = new Intent(getApplicationContext(), PlayerActivity.class);
        startActivity(intent);

    } //Bottom bar hitbox

    public void stopPlaying(View view) {
        MusicPlayer.stopPlaying();

        MusicPlayer.colorPreviousSongInGrey();
    }

    public void previousSong(View view) {
        Song song = FragmentSongs.findPreviousSongById(MusicPlayer.currentSong.getId());
        MusicPlayer.colorPreviousSongInGrey();

        for(int i = 0; i < FragmentSongs.listView.getCount(); i++) {
            int songId = Integer.valueOf(((TextView) getViewByPosition(i).findViewById(R.id.song_id)).getText().toString());
            if(songId == song.getId()) {
                MusicPlayer.colorSongPlaying(new TextView[]{((TextView) getViewByPosition(i).findViewById(R.id.song_name)), ((TextView) getViewByPosition(i).findViewById(R.id.song_artist))});
            }
        }

        FragmentSongs.getInstance().updateBottomBar(song);

        MusicPlayer.playSong(MainActivity.mainActivity, song);
    }

    public void nextSong(View view) {
        Song song = FragmentSongs.findNextSongById(MusicPlayer.currentSong.getId());
        MusicPlayer.colorPreviousSongInGrey();

        for(int i = 0; i < FragmentSongs.listView.getCount(); i++) {
            int songId = Integer.valueOf(((TextView) getViewByPosition(i).findViewById(R.id.song_id)).getText().toString());
            if(songId == song.getId()) {
                MusicPlayer.colorSongPlaying(new TextView[]{((TextView) getViewByPosition(i).findViewById(R.id.song_name)), ((TextView) getViewByPosition(i).findViewById(R.id.song_artist))});
            }
        }

        FragmentSongs.getInstance().updateBottomBar(song);

        MusicPlayer.playSong(MainActivity.mainActivity, song);
    }

    public static View getViewByPosition(int position) {
        final int firstListItemPosition = FragmentSongs.listView.getFirstVisiblePosition();
        final int lastListItemPosition =firstListItemPosition + FragmentSongs.listView.getChildCount() - 1;

        if (position < firstListItemPosition || position > lastListItemPosition ) {
            return FragmentSongs.listView.getAdapter().getView(position, FragmentSongs.listView.getChildAt(position), FragmentSongs.listView);
        } else {
            final int childIndex = position - firstListItemPosition;
            return FragmentSongs.listView.getChildAt(childIndex);
        }
    }
}

