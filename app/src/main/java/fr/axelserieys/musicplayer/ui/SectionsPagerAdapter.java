package fr.axelserieys.musicplayer.ui;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.widget.Toast;

import fr.axelserieys.musicplayer.R;
import fr.axelserieys.musicplayer.fragments.FragmentAlbums;
import fr.axelserieys.musicplayer.fragments.FragmentArtists;
import fr.axelserieys.musicplayer.fragments.FragmentCategories;
import fr.axelserieys.musicplayer.fragments.FragmentPlaylists;
import fr.axelserieys.musicplayer.fragments.FragmentSongs;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    @StringRes
    private static final int[] TAB_TITLES = new int[]{R.string.tab_text_1, R.string.tab_text_2, R.string.tab_text_3, R.string.tab_text_4, R.string.tab_text_5};
    private final Context mContext;

    public SectionsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    /**
     * getItem is called to instantiate the fragment for the given page.
     * @param position index of the tab
     * @return Fragment
     */
    @Override
    public Fragment getItem(int position) {
        switch(position) {
            case 0:
                return FragmentSongs.newInstance(mContext);
            case 1:
                return new FragmentArtists().newInstance(mContext);
            case 2:
                return new FragmentAlbums().newInstance(mContext);
            case 3:
                return new FragmentPlaylists().newInstance(mContext);
            case 4:
                return new FragmentCategories();
            default:
                return null;
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        // Show 5 total pages.
        return TAB_TITLES.length;
    }
}