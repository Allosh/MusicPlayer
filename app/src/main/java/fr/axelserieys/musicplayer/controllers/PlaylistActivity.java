package fr.axelserieys.musicplayer.controllers;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fr.axelserieys.musicplayer.Activities.MainActivity;
import fr.axelserieys.musicplayer.R;
import fr.axelserieys.musicplayer.adapters.SongAdapter;
import fr.axelserieys.musicplayer.database.DatabaseHelper;
import fr.axelserieys.musicplayer.fragments.FragmentPlaylists;
import fr.axelserieys.musicplayer.fragments.FragmentSongs;
import fr.axelserieys.musicplayer.models.Song;

public class PlaylistActivity extends AppCompatActivity {
    private ListView listView;
    public List<Song> selected = new ArrayList<Song>();
    private TextView countSelected;
    private boolean tvReady = false;
    public static PlaylistActivity playlistActivity;
    private Handler handler = new Handler();

    private Runnable updateCountDisplayTask = new Runnable() {
        @Override
        public void run() {
            countSelected.setText(""+selected.size());
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playlist);

        listView = findViewById(R.id.addPlaylistSongsList);
        listView.setAdapter(new SongAdapter(this, true));

        countSelected = findViewById(R.id.countSelected);
        tvReady = true;

        playlistActivity = this;
    }

    private boolean inArray(int id) {
        for(Song song : selected)
            if(song.getId() == id)
                return true;
        return false;
    }

    public void onClick(final View view) {
        int id = Integer.valueOf(((TextView) view.findViewById(R.id.song_id)).getText().toString());

        if(inArray(id)) {
            view.setBackgroundColor(Color.parseColor("#FFFFFF"));
            ((TextView) view.findViewById(R.id.song_name)).setTextColor(Color.parseColor("#757575"));
            ((TextView) view.findViewById(R.id.song_artist)).setTextColor(Color.parseColor("#757575"));
            handler.post(new UnselectSongTask(id));
            //unselectSong(id);
        } else {
            view.setBackgroundColor(Color.parseColor("#005c96"));
            ((TextView) view.findViewById(R.id.song_name)).setTextColor(Color.parseColor("#FFFFFF"));
            ((TextView) view.findViewById(R.id.song_artist)).setTextColor(Color.parseColor("#FFFFFF"));
            handler.post(new SelectSongTask(id));
            //selectSong(id);
        }

        handler.post(updateCountDisplayTask);
    }

    public void store(View view) {
        String name = ((EditText) findViewById(R.id.etName)).getText().toString();
        if(name.length() > 2) {
            int playlist_id = DatabaseHelper.getInstance().addPlaylist(name);

            for (Song song : selected) {
                DatabaseHelper.getInstance().linkSongPlaylist(song.getId(), playlist_id);
                Log.d("selected", song.getTitle());
            }

            FragmentPlaylists.initPlaylists();

            finish();
        } else {
            MainActivity.toast("Error: the name must be at least 3 characters long");
        }
    }
}

class UnselectSongTask implements Runnable {
    private int id;
    UnselectSongTask(int id) { this.id = id; }

    public void run() {
        int toremove = -1;
        for(int i = 0; i < PlaylistActivity.playlistActivity.selected.size(); i++) {
            if(PlaylistActivity.playlistActivity.selected.get(i).getId() == id) {
                toremove = 0;
            }
        }

        PlaylistActivity.playlistActivity.selected.remove(toremove);
    }
}

class SelectSongTask implements Runnable {
    private int id;
    SelectSongTask(int id) { this.id = id; }

    public void run() {
        PlaylistActivity.playlistActivity.selected.add(FragmentSongs.findSongById(id, true));
    }
};