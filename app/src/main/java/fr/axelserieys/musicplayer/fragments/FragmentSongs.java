package fr.axelserieys.musicplayer.fragments;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collections;
import java.util.Comparator;

import fr.axelserieys.musicplayer.Activities.MainActivity;
import fr.axelserieys.musicplayer.R;
import fr.axelserieys.musicplayer.Activities.SongEditActivity;
import fr.axelserieys.musicplayer.adapters.SongAdapter;
import fr.axelserieys.musicplayer.models.Album;
import fr.axelserieys.musicplayer.models.Artist;
import fr.axelserieys.musicplayer.models.Song;

public class FragmentSongs extends Fragment {

    private Context context;
    private static FragmentSongs fragmentSongs = null;
    private static MainActivity mActivity;
    public static ListView listView;
    private static View rootView = null;

    public static FragmentSongs newInstance(Context context) {
        if(fragmentSongs == null) {
            fragmentSongs = new FragmentSongs();
            fragmentSongs.context = context;
        }

        mActivity = MainActivity.getInstance();

        initMusic();
        getMusic("");

        return fragmentSongs;
    }

    public static FragmentSongs getInstance() {
        return fragmentSongs;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_songs, container, false);

        listView = rootView.findViewById(R.id.songs_list);
        listView.setAdapter(new SongAdapter(context, false));

        registerForContextMenu(listView);

        return rootView;
    }

    public static void initMusic() {
        ContentResolver contentResolver = MainActivity.mainActivity.getContentResolver();
        Uri songUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[] {MediaStore.Audio.Media.TITLE, MediaStore.Audio.Media.YEAR, MediaStore.Audio.Media.ALBUM, MediaStore.Audio.Media.ARTIST, MediaStore.Audio.Media.DATA, MediaStore.Audio.Media.ALBUM_ID};
        Cursor songCursor = contentResolver.query(songUri, projection, null, null, null);

        if (songCursor != null && songCursor.moveToFirst()) {
            int songTitle = songCursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
            int songYear = songCursor.getColumnIndex(MediaStore.Audio.Media.YEAR);
            int songAlbum = songCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM);
            int songArtist = songCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
            int songPath = songCursor.getColumnIndex(MediaStore.Audio.Media.DATA);
            int albumId = songCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID);

            do {
                String currentTitle = songCursor.getString(songTitle);
                String currentYear = songCursor.getString(songYear);
                String currentArtist = songCursor.getString(songArtist);
                String currentPath = songCursor.getString(songPath);
                String currentAlbum = songCursor.getString(songAlbum);
                int year = (currentYear == null) ? -1 : Integer.valueOf(currentYear);
                long currentAlbumId = songCursor.getLong(albumId);

                Artist artist = getOrCreateArtist(currentArtist);
                Album album = getOrCreateAlbum(currentAlbum);

                MainActivity.master_songs.add(new Song(currentPath, Song.getNewId(), currentTitle, artist, year, album));
            } while (songCursor.moveToNext());
        }

        Collections.sort(MainActivity.master_songs, new SongComparator());
    }

    public static void getMusic(String query) {
        MainActivity.songs.clear();

        for(Song song : MainActivity.master_songs) {
            if ((song.getTitle() != null && song.getTitle().toLowerCase().contains(query.toLowerCase())) || (song.getArtist().getName() != null && song.getArtist().getName().toLowerCase().contains(query.toLowerCase()))) {
                MainActivity.songs.add(song);
            }
        }

        if(MainActivity.songs.size() == 0) {
            Toast.makeText(fragmentSongs.context, "Sorry, no music found", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        if(v.getId() == R.id.songs_list) {
            MenuInflater inflater = MainActivity.mainActivity.getMenuInflater();
            inflater.inflate(R.menu.song_menu, menu);
        }
    }

    public void editSong(Song song) {
        Intent intent = new Intent(context, SongEditActivity.class);

        intent.putExtra("song", song.getId());

        startActivity(intent);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Log.d("selected", "selected");
        switch(item.getItemId()) {
            case R.id.option_edit:
                View v = ((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).targetView;
                Song song = Song.getSongById(Integer.valueOf(((TextView) v.findViewById(R.id.song_id)).getText().toString()));
                editSong(song);
                break;
            case R.id.option_add_playlist:
                MainActivity.toast("add");
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private static Artist getOrCreateArtist(String name) {
        if(name == null) { name = "<unknown>"; }

        for(Artist artist : MainActivity.artists) {
            if(artist.getName().equals(name))
                return artist;
        }

        Artist temp = new Artist(name);
        MainActivity.artists.add(temp);
        return temp;
    }

    private static Album getOrCreateAlbum(String name) {
        if(name == null) { name = ""; }

        for(Album album : MainActivity.albums)
            if(album.getName().equals(name))
            if(album.getName().equals(name))
                return album;

        Album temp = new Album(name);
        MainActivity.albums.add(temp);
        return temp;
    }

    public static Song findSongById(int id, boolean usingMasterSongsArray) {
        if(usingMasterSongsArray) {
            for (Song song : MainActivity.master_songs)
                if (song.getId() == id)
                    return song;
            return null;
        } else {
            for (Song song : MainActivity.songs)
                if (song.getId() == id)
                    return song;
            return null;
        }
    }

    public static Song findNextSongById(int id) {
        for(int i = 0; i < MainActivity.songs.size(); i++) {
            if(MainActivity.songs.get(i).getId() == id) {
                return MainActivity.songs.get(i+1);
            }
        }
        return null;
    }

    public static Song findPreviousSongById(int id) {
        for(int i = 1; i < MainActivity.songs.size(); i++)  {
            if(MainActivity.songs.get(i).getId() == id)
                return MainActivity.songs.get(i-1);
        }
        return null;
    }

    public void updateBottomBar(Song song) {
        ((TextView) mActivity.findViewById(R.id.bottomSongTitle)).setText(song.getTitle());
        ((TextView) mActivity.findViewById(R.id.bottomSongArtist)).setText(song.getArtist().getName());
    }
}

class SongComparator implements Comparator<Song> {

    @Override
    public int compare(Song left, Song right) {
        return left.getTitle().compareTo(right.getTitle());
    }
}
