package fr.axelserieys.musicplayer.Player;

import android.content.Context;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Handler;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import fr.axelserieys.musicplayer.Activities.MainActivity;
import fr.axelserieys.musicplayer.Activities.PlayerActivity;
import fr.axelserieys.musicplayer.Notifications.MusicNotification;
import fr.axelserieys.musicplayer.R;
import fr.axelserieys.musicplayer.models.Song;

public class MusicPlayer {
    public static boolean isPlaying = false;
    public static MediaPlayer player = new MediaPlayer();
    private static Context context;
    public static Song currentSong;

    public static TextView prevSongNameView = null;
    public static TextView prevSongArtistView = null;

    private static SeekBar seekBar = MainActivity.mainActivity.findViewById(R.id.seekbar);

    private static Handler seekbarHandler = new Handler();
    private static Runnable updateSeekbar = new Runnable() {
        @Override
        public void run() {
            seekBar.setProgress(player.getCurrentPosition());
            if(PlayerActivity.playerSeekbar != null) PlayerActivity.playerSeekbar.setProgress(player.getCurrentPosition());

            MainActivity.musicNotificationManager.updateProgressBar(player.getCurrentPosition() * 100 / player.getDuration());
            MainActivity.musicNotificationManager.updateDescription(player.getCurrentPosition(), player.getDuration());
            MainActivity.musicNotificationManager.updateTitle(currentSong.getTitle() + " - " + currentSong.getArtist().getName());

            PlayerActivity.updateCurrentTime(player.getCurrentPosition());

            seekbarHandler.postDelayed(this, 50);
        }
    };

    static {
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(fromUser)
                    player.seekTo(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });
    }


    public static void playSong(Context context, Song song) {
        MusicPlayer.context = context;
        MusicPlayer.currentSong = song;
        player.reset();
        try {
            player.setDataSource(song.getPath());
            player.prepare();
            seekBar.setMax(player.getDuration());

        } catch (IOException e) { e.printStackTrace(); }

        play();

        Toast.makeText(context, "Now playing: " + song.getTitle() + " by " + song.getArtist().getName() + " album " + song.getAlbum().getName(), Toast.LENGTH_SHORT).show();

        player.setOnCompletionListener(new onCompletion());
    }

    public static void play() {
        player.start();
        MusicNotification.getInstance().builder.setOngoing(true);
        MusicNotification.getInstance().show();
        seekbarHandler.postDelayed(updateSeekbar, 0);
        isPlaying = true;
    }

    public static void pause() {
        player.pause();
        MusicNotification.getInstance().builder.setOngoing(false);
        MusicNotification.getInstance().show();
        seekbarHandler.removeCallbacks(updateSeekbar);
        isPlaying = false;
    }

    public static void stopPlaying() {
        seekbarHandler.removeCallbacks(updateSeekbar);
        MainActivity.mainActivity.hideBottomBar();
        MusicNotification.getInstance().hide();
        player.stop();
        isPlaying = false;
    }

    public static void forward() {
        player.seekTo(player.getCurrentPosition() + 10 * 1000);
        MainActivity.toast(player.getDuration() + "-" + 10*1000);
    }

    public static void backward() {
        player.seekTo(player.getCurrentPosition() - 10 * 1000);
    }

    public static void colorSongPlaying(TextView[] tv) {
        tv[0].setTextColor(Color.parseColor("#FF0000"));
        tv[1].setTextColor(Color.parseColor("#FF0000"));

        prevSongNameView = tv[0];
        prevSongArtistView = tv[1];
    }

    public static void colorPreviousSongInGrey() {
        if(prevSongArtistView != null) {
            prevSongNameView.setTextColor(Color.parseColor("#757575"));
            prevSongArtistView.setTextColor(Color.parseColor("#757575"));
        }
    }
}

class onCompletion implements MediaPlayer.OnCompletionListener {

    @Override
    public void onCompletion(MediaPlayer mp) {
        MainActivity.mainActivity.nextSong(null);
    }
}
