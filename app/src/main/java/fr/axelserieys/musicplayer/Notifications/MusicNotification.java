package fr.axelserieys.musicplayer.Notifications;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import fr.axelserieys.musicplayer.Activities.MainActivity;
import fr.axelserieys.musicplayer.Activities.PlayerActivity;
import fr.axelserieys.musicplayer.R;

public class MusicNotification {
    public static MusicNotification musicNotification = null;
    public NotificationCompat.Builder builder;
    private NotificationManagerCompat notificationManager;
    public static final int PROGRESS_MAX = 100;

    private MusicNotification() {
        Intent intent = new Intent(MainActivity.mainActivity, PlayerActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(MainActivity.mainActivity, 0, intent, 0);

        createNotificationChannel();
        notificationManager = NotificationManagerCompat.from(MainActivity.mainActivity);

        builder = new NotificationCompat.Builder(MainActivity.mainActivity, "1")
                .setSmallIcon(R.drawable.musicdefault)
                .setContentTitle("Mon titre\n")
                .setContentText("Mon contenu")
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .setOngoing(true)
                .setProgress(100, 0, false);
    }

    public static MusicNotification getInstance() {
        if(musicNotification == null)
            musicNotification = new MusicNotification();

        return musicNotification;
    }

    private void createNotificationChannel() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "name";
            String description = "desc";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("1", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = MainActivity.mainActivity.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);

        }
    }

    public void show() {
        notificationManager.notify(0710, builder.build());
    }

    public void hide() {
        notificationManager.cancel(0710);
    }

    public void updateProgressBar(int value) {
        builder.setProgress(PROGRESS_MAX, value, false);
        show();
    }

    public void updateDescription(int current, int total) {
        builder.setContentText(PlayerActivity.millisToString(current) + "/" + PlayerActivity.millisToString(total));
        show();
    }

    public void updateTitle(String title) {
        builder.setContentTitle(title);
        show();
    }
}
