package fr.axelserieys.musicplayer.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import fr.axelserieys.musicplayer.Activities.MainActivity;
import fr.axelserieys.musicplayer.R;
import fr.axelserieys.musicplayer.fragments.FragmentAlbums;
import fr.axelserieys.musicplayer.models.Album;
import fr.axelserieys.musicplayer.models.Song;

public class AlbumAdapter extends BaseAdapter implements View.OnClickListener {

    private static Context context;
    private LayoutInflater inflater;

    public AlbumAdapter(Context context) {
        AlbumAdapter.context = context;

        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return MainActivity.albums.size();
    }

    @Override
    public Album getItem(int position) {
        return MainActivity.albums.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.adapter_album, null);

        Album currentAlbum = getItem(position);
        String currentName = currentAlbum.getName();
        int currentCount = Song.getSongsCountForAlbum(currentAlbum.getId());

        TextView albumNameView = view.findViewById(R.id.album_name);
        albumNameView.setText(currentName);

        TextView albumCountView = view.findViewById(R.id.album_count);
        albumCountView.setText(""+currentCount);

        TextView albumIdView = view.findViewById(R.id.album_id);
        albumIdView.setText(""+currentAlbum.getId());

        view.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        Album album = FragmentAlbums.findAlbumById(Integer.valueOf(((TextView) v.findViewById(R.id.album_id)).getText().toString()));

        SongAdapter.showMusicsForAlbum(album);

        MainActivity.tabs.getTabAt(0).select();
    }
}
