
package fr.axelserieys.musicplayer.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import fr.axelserieys.musicplayer.Activities.MainActivity;
import fr.axelserieys.musicplayer.R;
import fr.axelserieys.musicplayer.database.DatabaseHelper;
import fr.axelserieys.musicplayer.fragments.FragmentPlaylists;
import fr.axelserieys.musicplayer.models.Playlist;

public class PlaylistAdapter extends BaseAdapter implements View.OnClickListener, View.OnLongClickListener {

    private LayoutInflater inflater;
    private Context context;

    public PlaylistAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public int getCount() {
        return MainActivity.playlists.size();
    }

    @Override
    public Playlist getItem(int position) {
        return MainActivity.playlists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.adapter_playlist, viewGroup, false);

        ((TextView) view.findViewById(R.id.playlist_id)).setText(""+getItem(position).getId());
        ((TextView) view.findViewById(R.id.playlist_name)).setText(getItem(position).getName());
        ((TextView) view.findViewById(R.id.playlist_count)).setText(""+DatabaseHelper.getInstance().getSongsCountForPlaylist(getItem(position)));

        view.setOnLongClickListener(this);
        view.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(final View v) {
        Playlist playlist = Playlist.getPlaylistById(Integer.valueOf(((TextView) v.findViewById(R.id.playlist_id)).getText().toString()));

        SongAdapter.showMusicsForPlaylist(playlist);

        MainActivity.tabs.getTabAt(0).select();
    }

    @Override
    public boolean onLongClick(final View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setTitle("Confirmation");
        builder.setMessage(Html.fromHtml("Do you really want to delete the playlist <b>" + ((TextView) v.findViewById(R.id.playlist_name)).getText() + "</b> that has " + ((TextView) v.findViewById(R.id.playlist_count)).getText() + " songs ?"));
        builder.setPositiveButton("Confirm",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DatabaseHelper.getInstance().deletePlaylist(Integer.valueOf(((TextView) v.findViewById(R.id.playlist_id)).getText().toString()));
                        MainActivity.toast("The playlist has been deleted successfully");
                        FragmentPlaylists.initPlaylists();
                        notifyDataSetChanged();
                    }
                });
        builder.setNegativeButton("Do not delete",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.toast("The playlist has not been deleted");
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();

        return true;
    }
}
