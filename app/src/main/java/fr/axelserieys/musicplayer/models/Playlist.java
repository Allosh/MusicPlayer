package fr.axelserieys.musicplayer.models;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import fr.axelserieys.musicplayer.Activities.MainActivity;
import fr.axelserieys.musicplayer.database.DatabaseHelper;

public class Playlist {
    private int id;
    private String name;

    public Playlist(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() { return this.id; }
    public String getName() { return this.name; }

    public static Playlist getPlaylistById(int id) {
        for(Playlist playlist : MainActivity.playlists)
            if(playlist.getId() == id)
                return playlist;
        return null;
    }

    public static List<Playlist> getAllPlaylists(Context context) {
        List<Playlist> playlists = new ArrayList<Playlist>();

        SQLiteDatabase db = DatabaseHelper.getInstance().getReadableDatabase();

        Cursor c = db.rawQuery("SELECT * FROM playlists", null);
        c.moveToFirst();

        while(!c.isAfterLast()) {
            Playlist p = new Playlist(Integer.valueOf(c.getString(0)), c.getString(1));

            playlists.add(p);

            c.moveToNext();
        }

        return playlists;
    }
}
