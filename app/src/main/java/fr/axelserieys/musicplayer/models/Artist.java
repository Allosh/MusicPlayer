package fr.axelserieys.musicplayer.models;

public class Artist {
    private int id;
    private static int currentId = 0;
    private String name;

    public Artist(String name) {
        this.name = name;
        this.id = currentId;
        currentId++;
    }

    public String getName() { return this.name; }
    public int getId() { return this.id; }
}
