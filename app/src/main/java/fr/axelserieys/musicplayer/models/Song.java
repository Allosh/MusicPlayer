package fr.axelserieys.musicplayer.models;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import fr.axelserieys.musicplayer.Activities.MainActivity;

public class Song {

    private String title;
    private Artist artist;
    private String path;
    private int year;
    private int id;
    private Album album;
    private Bitmap icon;

    private static int currentId = 0;

    public Song(String path, int id, String title, Artist artist, int year, Album album) {
        this.path = path;
        this.id = id;
        this.title = title;
        this.artist = artist;
        this.album = album;
        this.year = year;
    }

    public String getTitle() {
        return this.title;
    }

    public int getYear() { return this.year; }

    public Artist getArtist() {
        return this.artist;
    }

    public Album getAlbum() { return this.album; }

    public String getPath() { return this.path; }

    public int getId() { return this.id; }

    public Bitmap getIcon() { return this.icon; }

    public static int getNewId() {
        currentId += 1;
        return currentId;
    }

    public static Song getSongById(int id) {
        for(Song song : MainActivity.master_songs)
            if(song.getId() == id)
                return song;
        return null;
    }

    public static List<Song> getSongsForArtist(int id) {
        List<Song> songs = new ArrayList<Song>();

        for(Song song : MainActivity.master_songs) {
            if(song.getArtist().getId() == id) {
                songs.add(song);
            }
        }

        return songs;
    }

    public static int getSongsCountForArtist(int id) {
        int ret = 0;

        for(Song song : MainActivity.master_songs)
            if(song.getArtist().getId() == id)
                ret++;

        return ret;
    }

    public static List<Song> getSongsForAlbum(int id) {
        List<Song> songs = new ArrayList<Song>();

        for(Song song : MainActivity.master_songs) {
            if(song.getAlbum().getId() == id) {
                songs.add(song);
            }
        }

        return songs;
    }

    public static int getSongsCountForAlbum(int id) {
        int ret = 0;

        for(Song song : MainActivity.master_songs)
            if(song.getAlbum().getId() == id)
                ret++;

        return ret;
    }
}
